﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_3_4
{
    class ConsoleLogger: Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine(DateTime.Now + "-> CPU load: " + provider.CPULoad);
            Console.WriteLine("Available RAM: " + provider.AvailableRAM);
        }
    }
}
