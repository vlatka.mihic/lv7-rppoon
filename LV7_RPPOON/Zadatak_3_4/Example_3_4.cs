﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_3_4
{
    class Example_3_4: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            SystemDataProvider dataProvider = new SystemDataProvider();
            Logger filelogger = new FileLogger(@"c:\Users\Vlatka\source\repos\LV7_RPPOON\LV7_RPPOON\Zadatak_3_4\LV7_3_4.txt");
            Logger consolelogger = new ConsoleLogger();

            dataProvider.Attach(filelogger);
            dataProvider.Attach(consolelogger);
            while(true)
            {
                float availableRAM = dataProvider.GetAvailableRAM();
                float CPULoad = dataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
