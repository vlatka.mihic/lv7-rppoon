﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;

namespace LV7_RPPOON.Zadatak_3_4
{
    class SystemDataProvider: SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            //if (currentLoad != this.previousCPULoad)
            //{
            //    this.Notify();
            //}
            if (currentLoad > (this.previousCPULoad * (float)1.1) || currentLoad < (this.previousCPULoad * (float)0.9))
            {
                this.Notify();
            }

            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentRAM = this.AvailableRAM;
            //if (currentRAM != this.previousRAMAvailable)
            //{
            //    this.Notify();
            //}

            if (currentRAM > (this.previousRAMAvailable * (float)1.1) || currentRAM < (this.previousRAMAvailable * (float)0.9))
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentRAM;
            return currentRAM;
        }

    }
}
