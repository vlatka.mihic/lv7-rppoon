﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_1_2
{
    class LinearSearch: SearchStrategy
    {
        public override void Search(double[] array, double element)
        {
            bool result = false;
            int index = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == element)
                {
                    result = true;
                    index = i + 1;
                }
            }
            SearchResult(result, element, index);
        }
    }
}
