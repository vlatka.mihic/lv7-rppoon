﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_1_2
{
    class Example_2: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            double[] array = { 4, 5, 23, 56, 12, 1, 7, 86, 28, 55, 100 };
            NumberSequence sequence = new NumberSequence(array);

            SearchStrategy strategy = new LinearSearch();
            sequence.SetSearchStrategy(strategy);

            Console.WriteLine("Enter element to search: ");
            double element = Convert.ToDouble(Console.ReadLine());

            sequence.Search(element);
        }
    }
}
