﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_1_2
{
    class Example_1: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            double[] array = { 4, 5, 23, 56, 12, 1, 7, 86, 28, 55, 100 };
            NumberSequence sequence = new NumberSequence(array);

            SortStrategy strategy = new BubbleSort();
            sequence.SetSortStrategy(strategy);

            sequence.Sort();
            Console.WriteLine("Bubble sort:\n");
            Console.WriteLine(sequence);

            sequence.InsertAt(4, 42);
            Console.WriteLine("Inserted number 42 at index 4!\n");
            strategy = new CombSort();
            sequence.SetSortStrategy(strategy);

            sequence.Sort();
            Console.WriteLine("Combo sort:\n");
            Console.WriteLine(sequence);

            sequence.InsertAt(7, 35);
            Console.WriteLine("Inserted number 35 at index 7!\n");
            strategy = new SequentialSort();
            sequence.SetSortStrategy(strategy);

            sequence.Sort();
            Console.WriteLine("Sequential sort:\n");
            Console.WriteLine(sequence);
        }
    }
}
