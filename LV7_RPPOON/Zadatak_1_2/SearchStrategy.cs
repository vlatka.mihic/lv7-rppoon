﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_1_2
{
    abstract class SearchStrategy
    {
        public abstract void Search(double[] array, double element);
        protected void SearchResult(bool result, double element, int index)
        {
            if (result)
            {
                Console.WriteLine("Search successful");
                Console.WriteLine("Element {0} found at location {1}\n", element, index);
            }
            else
            {
                Console.WriteLine("Search unsuccessful");
            }
        }
    }
}
