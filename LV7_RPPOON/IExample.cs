﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON
{
    interface IExample
    {
        System.String Name { get; }
        void Run();
    }
}
