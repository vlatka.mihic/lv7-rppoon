﻿using LV7_RPPOON.Zadatak_1_2;
using LV7_RPPOON.Zadatak_3_4;
using LV7_RPPOON.Zadatak_5_6_7;
using System;
using System.Collections.Generic;

namespace LV7_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IExample> examples = new List<IExample>()
            {
                new Example_1(),
                new Example_2(),
                //new Example_3_4(),
                new Example_5(),
                new Example_6(),
                new Example_7()
            };

            foreach (IExample example in examples)
            {
                PrintUtilities.PrintStart(example.Name);
                example.Run();
                PrintUtilities.PrintEnd();
            }
        }
    }
}
