﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_5_6_7
{
    class Example_7: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public string RoundDouble(double number)
        {
            if (double.IsNaN(number)) { return "Some of the items are not for rent!"; }
            return (Math.Round(number, 3)).ToString();
        }
        public void Run()
        {
            Cart cart = new Cart();
            cart.AddItem(new DVD("The Godfather", DVDType.MOVIE, 132.54));
            cart.AddItem(new DVD("Software..", DVDType.SOFTWARE, 287.45));
            cart.AddItem(new Book("The fault in our stars", 75.87));
            cart.AddItem(new Book("A Game of Thrones: A Song of Ice and Fire", 176.98));

            BuyVisitor visitor = new BuyVisitor();
            cart.SetVisitor(visitor);

            Console.WriteLine(cart);
            Console.WriteLine(" -> Price with PDV: " + RoundDouble(cart.Accept()));

            RentVisitor rentVisitor = new RentVisitor();
            cart.SetVisitor(rentVisitor);
            Console.WriteLine(" -> Renting price: " + RoundDouble(cart.Accept()));


        }
    }
}
