﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_5_6_7
{
    class Example_6: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public string RoundDouble(double number)
        {
            if(double.IsNaN(number)) { return "Not for rent!"; }
            return (Math.Round(number, 3)).ToString();
        }
        public void Run()
        {
            DVD dVD = new DVD("The Godfather", DVDType.MOVIE, 132.54);
            DVD dVD1 = new DVD("Software..", DVDType.SOFTWARE, 287.45);
            Book book1 = new Book("The fault in our stars", 75.87);
            Book book2 = new Book("A Game of Thrones: A Song of Ice and Fire", 176.98);

            RentVisitor visitor = new RentVisitor();
            Console.WriteLine(dVD);
            Console.WriteLine(" -> Renting price: " + RoundDouble(dVD.Accept(visitor)));
            Console.WriteLine(dVD1);
            Console.WriteLine(" -> Renting price: " + RoundDouble(dVD1.Accept(visitor)));
            Console.WriteLine(book1);
            Console.WriteLine(" -> Renting price: " + RoundDouble(book1.Accept(visitor)));
            Console.WriteLine(book2);
            Console.WriteLine(" -> Renting price: " + RoundDouble(book2.Accept(visitor)));


        }
    }
}
