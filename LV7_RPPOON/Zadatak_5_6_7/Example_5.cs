﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7_RPPOON.Zadatak_5_6_7
{
    class Example_5: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public string RoundDouble(double number)
        {
            return (Math.Round(number, 3)).ToString();
        }
        public void Run()
        {
            DVD dVD = new DVD("The Godfather", DVDType.MOVIE, 132.54);
            Book book1 = new Book("The fault in our stars", 75.87);
            Book book2 = new Book("A Game of Thrones: A Song of Ice and Fire", 176.98);

            BuyVisitor visitor = new BuyVisitor();

            Console.WriteLine(dVD);
            Console.WriteLine(" -> Price with PDV: " + RoundDouble(dVD.Accept(visitor)));
            Console.WriteLine(book1);
            Console.WriteLine(" -> Price with PDV: " + RoundDouble(book1.Accept(visitor)));
            Console.WriteLine(book2);
            Console.WriteLine(" -> Price with PDV: " + RoundDouble(book2.Accept(visitor)));


        }
    }
}
