﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV7_RPPOON.Zadatak_5_6_7
{
    class Cart
    {
        private List<IItem> Items;
        private IVisitor visitor;

        public Cart() { this.Items = new List<IItem>(); }
        public void SetVisitor(IVisitor visitor)
        {
            this.visitor = visitor;
        }
        public void AddItem(IItem item)
        {
            Items.Add(item);
        }
        public void RemoveItem(IItem item)
        {
            Items.Remove(item);
        }
        public override string ToString()
        {
            string output = "";
            foreach (IItem item in Items)
            {
                output += item.ToString() + "\n";
            }
            return output;
        }
        public double Accept()
        {
            double temp = 0;
            foreach (IItem item in Items)
            {
                temp += item.Accept(visitor);
            }
            return temp;
        }
    }
}
